import mainstore from "./store/store";
import incrementMicro2 from "./actions/counter2";

const microApps = [
  {
    name: "micro-react1", // app name registered
    entry: "//micro-app2.netlify.app",
    container: "#subapp-viewport",
    activeRule: "/micro-react1",
    props: {
      mainstore,
    },
  },
  {
    name: "micro-react2", // app name registered
    entry: "//micro-react1.onrender.com",
    container: "#subapp-viewport",
    activeRule: "/micro-react2",
    props: {
      mainstore,
      incrementMicro2,
    },
  },
];

export default microApps;
